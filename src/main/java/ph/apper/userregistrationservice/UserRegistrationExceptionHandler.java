package ph.apper.userregistrationservice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class UserRegistrationExceptionHandler {

    @ExceptionHandler({InvalidUserRegistrationException.class, UserVerificationException.class})
    public ResponseEntity<Map<String, String>> handleLogicException(Exception e) {
        Map<String, String> response = new HashMap<>();
        response.put("message", "Invalid user registration: " + e.getMessage());

        return ResponseEntity.badRequest().body(response);
    }

}
