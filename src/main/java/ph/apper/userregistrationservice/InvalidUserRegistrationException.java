package ph.apper.userregistrationservice;

public class InvalidUserRegistrationException extends Exception {
    public InvalidUserRegistrationException(String message) {
        super(message);
    }

    public InvalidUserRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
