package ph.apper.userregistrationservice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.Map;

@RestController
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;
    private final VerificationService verificationService;

    public UserController(UserService userService, VerificationService verificationService) {
        this.userService = userService;
        this.verificationService = verificationService;
    }

    @PostMapping(path = "register")
    public ResponseEntity<UserRegistrationResponse> register(
            @Valid @RequestBody UserRegistration request,
            @RequestHeader("reference-number") String referenceNumber) throws InvalidUserRegistrationException {

        LOGGER.info("Processing user registration with reference number {}", referenceNumber);

        User registeredUser = userService.register(request);

        UserRegistrationResponse response = new UserRegistrationResponse();
        Map<String, String> verificationCodes = verificationService.getVerificationCodes();
        String verificationCode = verificationCodes.get(registeredUser.getEmail());
        response.setVerificationCode(verificationCode);

        LOGGER.info("User registration with reference number {} done!", referenceNumber);

        return ResponseEntity.created(URI.create(registeredUser.getId())).body(response);
    }

}
