package ph.apper.userregistrationservice;

import lombok.Data;

@Data
public class UserRegistrationResponse {
    private String verificationCode;
}
