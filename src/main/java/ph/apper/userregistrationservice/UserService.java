package ph.apper.userregistrationservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final List<User> users = new ArrayList<>();

    private final IdService idService;
    private final VerificationService verificationService;

    public UserService(IdService idService, VerificationService verificationService) {
        this.idService = idService;
        this.verificationService = verificationService;
    }

    public User register(UserRegistration userRegistration) throws InvalidUserRegistrationException {
        LocalDate parsedBirthDate = LocalDate.parse(userRegistration.getBirthDate());
        Period periodDiff = Period.between(parsedBirthDate, LocalDate.now());
        if (periodDiff.getYears() < 18) {
            throw new InvalidUserRegistrationException("age must be at least 18");
        }

        // get user id
        String userId = idService.getNextUserId();
        LOGGER.info("Generated User ID: {}", userId);

        // save registration details as User with ID
        User newUser = new User(userId);
        newUser.setFirstName(userRegistration.getFirstName());
        newUser.setLastName(userRegistration.getLastName());
        newUser.setBirthDate(parsedBirthDate);
        newUser.setEmail(userRegistration.getEmail());
        newUser.setPassword(userRegistration.getPassword());

        // create verification code
        String verificationCode = idService.generateCode(5);

        verificationService.addVerificationCode(newUser.getEmail(), verificationCode);
        users.add(newUser);

        return newUser;
    }

    public void verify(String email, String code) throws UserVerificationException {
        verificationService.verifyUser(email, code);
        getUser(email).setVerified(true);
    }

    public User getUser(String email) {
        return users.stream().filter(user -> email.equals(user.getEmail())).findFirst().get();
    }

    public int getNumberOfRegisteredUser() {
        return users.size();
    }

    public int getNumberOfVerificationCodes() {
        return verificationService.getVerificationCodes().size();
    }
}
