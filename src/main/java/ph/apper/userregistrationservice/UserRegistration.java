package ph.apper.userregistrationservice;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserRegistration {

    @Email(message = "email must be valid")
    @NotBlank(message = "email is required")
    private String email;

    @NotBlank(message = "firstName is required")
    private String firstName;

    @NotBlank(message = "lastName is required")
    private String lastName;

    @NotBlank(message = "password is required")
    private String password;

    @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}")//ISO Format YYYY-MM-DD
    @NotBlank(message = "birthDate is required")
    private String birthDate;
}
