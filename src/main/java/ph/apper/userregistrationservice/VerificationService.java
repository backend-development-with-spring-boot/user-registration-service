package ph.apper.userregistrationservice;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class VerificationService {
    private final Map<String, String> verificationCodes = new HashMap<>();

    public void addVerificationCode(String email, String code) {
        verificationCodes.put(email, code);
    }

    public void verifyUser(String email, String code) throws UserVerificationException {
        if (verificationCodes.containsKey(email) && code.equals(verificationCodes.get(email))) {
            verificationCodes.remove(email);
            return;
        }

        throw new UserVerificationException("Verification entry " + email + ": " + code + " not found");
    }

    public Map<String, String> getVerificationCodes() {
        return verificationCodes;
    }
}
